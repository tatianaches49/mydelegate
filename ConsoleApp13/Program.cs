﻿using System;
delegate void MyDelegate(int a, int b);
namespace ConsoleApp13
{

    class MyClass
    {
     
        public MyDelegate Method;
        public MyClass() { }
        public MyClass(MyDelegate md) {

            Method = md;
       
        }
    }
     


    class Program
    {
        static void Main(string[] args)
        {
            int a = 33; int b = 4;
            MyClass myClass = new MyClass();
            myClass.Method = (a,b) =>
            {
               Console.WriteLine(a - b);
            };
            myClass.Method(a, b);


            MyDelegate tmpDelegate = (a, b) => { Console.WriteLine(a * b); };
            tmpDelegate(a, b);

            MyClass myCl = new MyClass((a, b) => Console.WriteLine(a + b));
            myCl.Method(a,b);

        }
    }
}
